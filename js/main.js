(function () {
    "use strict";

    window.onload = function () {
        console.log("Starting window.onload function.");

        var gieteraudio = document.getElementById("gieteraudio");

        // Object variabelen
        var camera = document.getElementById("js--camera");
        var scene = document.getElementById("js--scene");

        var gieter = document.getElementById("gieter");
        var hark = document.getElementById("hark");

        var gieter_station = document.getElementById("gieter_station");
        var hark_station = document.getElementById("hark_station");

        // Basis variabelen
        var onkruid_weghalen = false;
        var onkruid_aanwezig = true;
        var hark_datetime = null;

        var zaadjes_planten = false; // Misschien niet ingebruik als er gebruik wordt gemaakt van een menu.
        var zaadjes_geplant = false;
        var dibber_datetime = null; // Misschien niet ingebruikt, kan ook gewoon met de hand.

        var water_geven = false;
        var water_gegeven = false;
        var gieter_datetime = null;


        var onkruid_weghalen2 = false;
        var onkruid_aanwezig2 = true;

        var zaadjes_planten2 = false; // Misschien niet ingebruik als er gebruik wordt gemaakt van een menu.
        var zaadjes_geplant2 = false;
        var dibber_datetime2 = null; // Misschien niet ingebruikt, kan ook gewoon met de hand.

        var water_geven2 = false;
        var water_gegeven2 = false;

        var onkruid_weghalen3 = false;
        var onkruid_aanwezig3 = true;

        var zaadjes_planten3 = false; // Misschien niet ingebruik als er gebruik wordt gemaakt van een menu.
        var zaadjes_geplant3 = false;
        var dibber_datetime3 = null; // Misschien niet ingebruikt, kan ook gewoon met de hand.

        var water_geven3 = false;
        var water_gegeven3 = false;

        var onkruid_weghalen4 = false;
        var onkruid_aanwezig4 = true;

        var zaadjes_planten4 = false; // Misschien niet ingebruik als er gebruik wordt gemaakt van een menu.
        var zaadjes_geplant4 = false;
        var dibber_datetime4 = null; // Misschien niet ingebruikt, kan ook gewoon met de hand.

        var water_geven4 = false;
        var water_gegeven4 = false;

        var perk1_plantkeuze;
        var perk2_plantkeuze;
        var perk3_plantkeuze;
        var perk4_plantkeuze;

        // perkjes
        var perk1 = document.getElementById("js--perk1");
        var perk2 = document.getElementById("js--perk2");
        var perk3 = document.getElementById("js--perk3");
        var perk4 = document.getElementById("js--perk4");

        // menu's
        var menu1 = document.getElementById("js--menu1");
        var menu2 = document.getElementById("js--menu2");
        var menu3 = document.getElementById("js--menu3");
        var menu4 = document.getElementById("js--menu4");

        //Animaties
        var harkAnimatie = document.getElementById("js--harkAnimatie");

        //text
        var stappen = document.getElementById("stappen");

        //menu 1
        var menu1_choice1 = document.getElementById("js--menu1_choice1");
        var menu1_plant1 = document.getElementById("wortel");
        var menu1_choice2 = document.getElementById("js--menu1_choice2");
        var menu1_plant2 = document.getElementById("radijs");
        var menu1_choice3 = document.getElementById("js--menu1_choice3");
        var menu1_plant3 = document.getElementById("ui");
        var menu1_choice4 = document.getElementById("js--menu1_choice4");
        var menu1_plant4 = document.getElementById("aardappel");
        //menu 2
        var menu2_choice1 = document.getElementById("js--menu2_choice1");
        var menu2_plant1 = document.getElementById("wortel2");
        var menu2_choice2 = document.getElementById("js--menu2_choice2");
        var menu2_plant2 = document.getElementById("radijs2");
        var menu2_choice3 = document.getElementById("js--menu2_choice3");
        var menu2_plant3 = document.getElementById("ui2");
        var menu2_choice4 = document.getElementById("js--menu2_choice4");
        var menu2_plant4 = document.getElementById("aardappel2");
        //menu 3
        var menu3_choice1 = document.getElementById("js--menu3_choice1");
        var menu3_plant1 = document.getElementById("wortel3");
        var menu3_choice2 = document.getElementById("js--menu3_choice2");
        var menu3_plant2 = document.getElementById("radijs3");
        var menu3_choice3 = document.getElementById("js--menu3_choice3");
        var menu3_plant3 = document.getElementById("ui3");
        var menu3_choice4 = document.getElementById("js--menu3_choice4");
        var menu3_plant4 = document.getElementById("aardappel3");
        //menu 4
        var menu4_choice1 = document.getElementById("js--menu4_choice1");
        var menu4_plant1 = document.getElementById("wortel4");
        var menu4_choice2 = document.getElementById("js--menu4_choice2");
        var menu4_plant2 = document.getElementById("radijs4");
        var menu4_choice3 = document.getElementById("js--menu4_choice3");
        var menu4_plant3 = document.getElementById("ui4");
        var menu4_choice4 = document.getElementById("js--menu4_choice4");
        var menu4_plant4 = document.getElementById("aardappel4");

        var omgeving = document.getElementById("omgeving");
        var spot = document.getElementById("js--spot")

        hark.onclick = function() {
            if (hark_datetime == null || Date.now() - hark_datetime > 2000) {
                console.log("Hark oppakken.");
                hark_datetime = Date.now();
                camera.appendChild(hark);
                hark.setAttribute("position", "1 -0.6 -2");
                hark.setAttribute("rotation", "270 140 50");
                spot.setAttribute("visible", "false");
                onkruid_weghalen = true;
                onkruid_weghalen2 = true;
                onkruid_weghalen3 = true;
                onkruid_weghalen4 = true;
                stappen.setAttribute("text", {
                  value: "Hark nu de onkruid weg"
                });
            } else {
                console.log("Kan de hark nu niet oppakken, wacht nog heel even.");
            }
        }

        hark_station.onclick = function() {
            if (Date.now() - hark_datetime > 2000) {
                console.log("Hark neerzetten.");
                hark_datetime = Date.now();
                camera.removeChild(hark);
                scene.appendChild(hark);
                onkruid_weghalen = false;
                onkruid_weghalen2 = false;
                onkruid_weghalen3 = false;
                onkruid_weghalen4 = false;
                stappen.setAttribute("text", {
                  value: "ga nu zaadjes planten in een perkje"
                });
            } else {
                console.log("Kan de hark nu niet wegzetten, wacht nog heel even.");
            }
        }

        gieter.onclick = function() {
            if (gieter_datetime == null || Date.now() - gieter_datetime > 1000) {
                console.log("Gieter oppakken.");
                gieter_datetime = Date.now();
                camera.appendChild(gieter);
                gieter.setAttribute("position", "1 -0.7 -1.2");
                water_geven = true;
                water_geven2 = true;
                water_geven3 = true;
                water_geven4 = true;
            } else {
                console.log("Kan de gieter nu niet oppakken, wacht nog heel even.");
            }
        }

        gieter_station.onclick = function() {
            if (Date.now() - gieter_datetime > 1000) {
                console.log("Gieter neerzetten.");
                gieter_datetime = Date.now();
                camera.removeChild(gieter);
                scene.appendChild(gieter);
                water_geven = false;
                water_geven2 = false;
                water_geven3 = false;
                water_geven4 = false;
            } else {
                console.log("Kan de gieter nu niet wegzetten, wacht nog heel even.");
            }
        }


        //oogsten van groenten en planten
        wortel.onclick = function() {
          camera.appendChild(wortel);
          wortel.setAttribute("visible", "true")
          wortel.setAttribute("position", "1 -0.7 -1.2");
        }

        radijs.onclick = function() {
          camera.appendChild(radijs);
          wortel.setAttribute("visible", "true")
          radijs.setAttribute("position", "1 -0.7 -1.2");
        }

        aardappel.onclick = function() {
          camera.appendChild(aardappel);
          wortel.setAttribute("visible", "true")
          aardappel.setAttribute("position", "1 -0.7 -1.2");
        }

        ui.onclick = function() {
          camera.appendChild(ui);
          wortel.setAttribute("visible", "true")
          ui.setAttribute("position", "1 -0.7 -1.2");
        }


        //groei functies perk1
        function growPerk_plant1() {
            menu1_plant1.setAttribute("position", "4 -0.8 -2");
        }
        function growPerk_plant1_2() {
            menu1_plant2.setAttribute("position", "3.8 0.2 -2");
        }
        function growPerk_plant1_3() {
            menu1_plant3.setAttribute("position", "4 0.2 -2");
        }
        function growPerk_plant1_4() {
            menu1_plant4.setAttribute("position", "4 0.34 -2");
        }

        //groei functies perk2
        function growPerk_plant2() {
            menu2_plant1.setAttribute("position", "4 -0.8 4");
        }
        function growPerk_plant2_2() {
            menu2_plant2.setAttribute("position", "3.8 0.2 4");
        }
        function growPerk_plant2_3() {
            menu2_plant3.setAttribute("position", "4 0.2 4");
        }
        function growPerk_plant2_4() {
            menu2_plant4.setAttribute("position", "4 0.34 4");
        }

        //groei functies perk3
        function growPerk_plant3() {
            menu3_plant1.setAttribute("position", "-2 -0.8 -2");
        }
        function growPerk_plant3_2() {
            menu3_plant2.setAttribute("position", "-1.8 0.2 -2");
        }
        function growPerk_plant3_3() {
            menu3_plant3.setAttribute("position", "-2 0.2 -2");
        }
        function growPerk_plant3_4() {
            menu3_plant4.setAttribute("position", "-2 0.34 -2");
        }

        //groei functies perk4
        function growPerk_plant4() {
            menu4_plant1.setAttribute("position", "-2 -0.8 4");
        }
        function growPerk_plant4_2() {
            menu4_plant2.setAttribute("position", "-1.8 0.2 4");
        }
        function growPerk_plant4_3() {
            menu4_plant3.setAttribute("position", "-2 0.2 4");
        }
        function growPerk_plant4_4() {
            menu4_plant4.setAttribute("position", "-2 0.34 4");
        }

        perk1.onclick = function() {
            if (onkruid_aanwezig){
                if (onkruid_aanwezig && onkruid_weghalen) {
                    console.log("ONKRUID VERNIETIGEN.");
                    var gras = document.getElementById("gras");
                    gras.setAttribute("position", "4 -1 -2.5");
                    onkruid_aanwezig = false;
                    stappen.setAttribute("text", {
                      value: "Zet de hark terug op het rode vlak"
                    });
                }
                if (onkruid_aanwezig == false) {
                    console.log("Er is geen onkruid.");
                }
                if (onkruid_weghalen == false) {
                    console.log("Je hebt geen hark.");
                }
            } else if (zaadjes_geplant == false) {
                menu1.setAttribute("visible", "true");
                menu1_choice1.onclick = function() {
                    menu1.setAttribute("visible", "false");
                    zaadjes_geplant = true;
                    perk1_plantkeuze = "wortel";
                    stappen.setAttribute("text", {
                      value: "Pak de gieter en geef de geplante zaadjes water"
                    });
                }
                menu1_choice2.onclick = function() {
                    menu1.setAttribute("visible", "false");
                    zaadjes_geplant = true;
                    perk1_plantkeuze = "radijs";
                    stappen.setAttribute("text", {
                      value: "Pak de gieter en geef de geplante zaadjes water"
                    });
                }
                menu1_choice3.onclick = function() {
                    menu1.setAttribute("visible", "false");
                    zaadjes_geplant = true;
                    perk1_plantkeuze = "ui";
                    stappen.setAttribute("text", {
                      value: "Pak de gieter en geef de geplante zaadjes water"
                    });
                }
                menu1_choice4.onclick = function() {
                    menu1.setAttribute("visible", "false");
                    zaadjes_geplant = true;
                    perk1_plantkeuze = "aardappel";
                    stappen.setAttribute("text", {
                      value: "Pak de gieter en geef de geplante zaadjes water"
                    });
                }
            } else if (water_gegeven == false) {
                if (water_gegeven == false && water_geven) {
                    console.log("WATER GEVEN.");
                    // gieteraudio.play();
                    this.setAttribute("color", "#453519");
                    water_gegeven = true;

                    if(perk1_plantkeuze === "wortel"){
                      setTimeout(growPerk_plant1, 2000);
                    }

                    if(perk1_plantkeuze === "radijs"){
                      setTimeout(growPerk_plant1_2, 2000);
                    }

                    if(perk1_plantkeuze === "ui"){
                      setTimeout(growPerk_plant1_3, 2000);
                    }
                    if(perk1_plantkeuze === "aardappel"){
                      setTimeout(growPerk_plant1_4, 2000);
                    }
                }
                if (water_gegeven) {
                    console.log("Dit perkje heeft geen water meer nodig.");
                }
                if (water_geven == false) {
                    console.log("Je hebt een gieter nodig om water te geven.");
                }
            }

        }

        perk2.onclick = function() {
            if (onkruid_aanwezig2){
                if (onkruid_aanwezig2 && onkruid_weghalen2) {
                    console.log("ONKRUID VERNIETIGEN.");
                    var gras1 = document.getElementById("gras1");
                    gras1.setAttribute("position", "4 -1 -2.5");
                    onkruid_aanwezig2 = false;
                }
                if (onkruid_aanwezig2 == false) {
                    console.log("Er is geen onkruid.");
                }
                if (onkruid_weghalen2 == false) {
                    console.log("Je hebt geen hark.");
                }
            } else if (zaadjes_geplant2 == false) {
              menu2.setAttribute("visible", "true");
              menu2_choice1.onclick = function() {
                  menu2.setAttribute("visible", "false");
                  zaadjes_geplant2 = true;
                  perk2_plantkeuze = "wortel";
                  stappen.setAttribute("text", {
                    value: "Pak de gieter en geef de geplante zaadjes water"
                  });
              }
              menu2_choice2.onclick = function() {
                  menu2.setAttribute("visible", "false");
                  zaadjes_geplant2 = true;
                  perk2_plantkeuze = "radijs";
                  stappen.setAttribute("text", {
                    value: "Pak de gieter en geef de geplante zaadjes water"
                  });
              }
              menu2_choice3.onclick = function() {
                  menu2.setAttribute("visible", "false");
                  zaadjes_geplant2 = true;
                  perk2_plantkeuze = "ui";
                  stappen.setAttribute("text", {
                    value: "Pak de gieter en geef de geplante zaadjes water"
                  });
              }
              menu2_choice4.onclick = function() {
                  menu2.setAttribute("visible", "false");
                  zaadjes_geplant2 = true;
                  perk2_plantkeuze = "aardappel";
                  stappen.setAttribute("text", {
                    value: "Pak de gieter en geef de geplante zaadjes water"
                  });
              }

            } else if (water_gegeven2 == false) {
                if (water_gegeven2 == false && water_geven2) {
                    console.log("WATER GEVEN.");
                    // gieteraudio.play();
                    this.setAttribute("color", "#453519");
                    water_gegeven2 = true;

                    if(perk2_plantkeuze === "wortel"){
                      setTimeout(growPerk_plant2, 2000);
                    }
                    if(perk2_plantkeuze === "radijs"){
                      setTimeout(growPerk_plant2_2, 2000);
                    }
                    if(perk2_plantkeuze === "ui"){
                      setTimeout(growPerk_plant2_3, 2000);
                    }
                    if(perk2_plantkeuze === "aardappel"){
                      setTimeout(growPerk_plant2_4, 2000);
                    }
                }
                if (water_gegeven2) {
                    console.log("Dit perkje heeft geen water meer nodig.");
                }
                if (water_geven2 == false) {
                    console.log("Je hebt een gieter nodig om water te geven.");
                }
            }
        }

        perk3.onclick = function() {
            if (onkruid_aanwezig3){
                if (onkruid_aanwezig3 && onkruid_weghalen3) {
                    console.log("ONKRUID VERNIETIGEN.");
                    var gras2 = document.getElementById("gras2");

                    gras2.setAttribute("position", "4 -1 -2.5");
                    onkruid_aanwezig3 = false;
                }
                if (onkruid_aanwezig3 == false) {
                    console.log("Er is geen onkruid.");
                }
                if (onkruid_weghalen3 == false) {
                    console.log("Je hebt geen hark.");
                }
            } else if (zaadjes_geplant3 == false) {
                menu3.setAttribute("visible", "true");
                menu3_choice1.onclick = function() {
                    menu3.setAttribute("visible", "false");
                    zaadjes_geplant3 = true;
                    perk3_plantkeuze = "wortel";
                    stappen.setAttribute("text", {
                      value: "Pak de gieter en geef de geplante zaadjes water"
                    });
                }
                menu3_choice2.onclick = function() {
                    menu3.setAttribute("visible", "false");
                    zaadjes_geplant3 = true;
                    perk3_plantkeuze = "radijs";
                    stappen.setAttribute("text", {
                      value: "Pak de gieter en geef de geplante zaadjes water"
                    });
                }
                menu3_choice3.onclick = function() {
                    menu3.setAttribute("visible", "false");
                    zaadjes_geplant3 = true;
                    perk3_plantkeuze = "ui";
                    stappen.setAttribute("text", {
                      value: "Pak de gieter en geef de geplante zaadjes water"
                    });
                }
                menu3_choice4.onclick = function() {
                    menu3.setAttribute("visible", "false");
                    zaadjes_geplant3 = true;
                    perk3_plantkeuze = "aardappel";
                    stappen.setAttribute("text", {
                      value: "Pak de gieter en geef de geplante zaadjes water"
                    });
                }
            } else if (water_gegeven3 == false) {
                if (water_gegeven3 == false && water_geven3) {
                    console.log("WATER GEVEN.");
                    // gieteraudio.play();
                    this.setAttribute("color", "#453519");
                    water_gegeven3 = true;

                    if(perk3_plantkeuze === "wortel"){
                      setTimeout(growPerk_plant3, 2000);
                    }
                    if(perk3_plantkeuze === "radijs"){
                      setTimeout(growPerk_plant3_2, 2000);
                    }
                    if(perk3_plantkeuze === "ui"){
                      setTimeout(growPerk_plant3_3, 2000);
                    }
                    if(perk3_plantkeuze === "aardappel"){
                      setTimeout(growPerk_plant3_4, 2000);
                    }
                }
                if (water_gegeven3) {
                    console.log("Dit perkje heeft geen water meer nodig.");
                }
                if (water_geven3 == false) {
                    console.log("Je hebt een gieter nodig om water te geven.");
                }
            }
        }

        perk4.onclick = function() {
            if (onkruid_aanwezig4){
                if (onkruid_aanwezig4 && onkruid_weghalen4) {
                    console.log("ONKRUID VERNIETIGEN.");
                    var gras3 = document.getElementById("gras3");

                    gras3.setAttribute("position", "4 -1 -2.5");

                    onkruid_aanwezig4 = false;
                }
                if (onkruid_aanwezig4 == false) {
                    console.log("Er is geen onkruid.");
                }
                if (onkruid_weghalen4 == false) {
                    console.log("Je hebt geen hark.");
                }
            } else if (zaadjes_geplant4 == false) {
                menu4.setAttribute("visible", "true");
                menu4_choice1.onclick = function() {
                    menu4.setAttribute("visible", "false");
                    zaadjes_geplant4 = true;
                    perk4_plantkeuze = "wortel";
                    stappen.setAttribute("text", {
                      value: "Pak de gieter en geef de geplante zaadjes water"
                    });
                }
                menu4_choice2.onclick = function() {
                    menu4.setAttribute("visible", "false");
                    zaadjes_geplant4 = true;
                    perk4_plantkeuze = "radijs";
                    stappen.setAttribute("text", {
                      value: "Pak de gieter en geef de geplante zaadjes water"
                    });
                }
                menu4_choice3.onclick = function() {
                    menu4.setAttribute("visible", "false");
                    zaadjes_geplant4 = true;
                    perk4_plantkeuze = "ui";
                    stappen.setAttribute("text", {
                      value: "Pak de gieter en geef de geplante zaadjes water"
                    });
                }
                menu4_choice4.onclick = function() {
                    menu4.setAttribute("visible", "false");
                    zaadjes_geplant4 = true;
                    perk4_plantkeuze = "aardappel";
                    stappen.setAttribute("text", {
                      value: "Pak de gieter en geef de geplante zaadjes water"
                    });
                }
            } else if (water_gegeven4 == false) {
                if (water_gegeven4 == false && water_geven4) {
                    console.log("WATER GEVEN.");
                    // gieteraudio.play();
                    this.setAttribute("color", "#453519");
                    water_gegeven4 = true;

                    if(perk4_plantkeuze === "wortel"){
                      setTimeout(growPerk_plant4, 2000);
                    }
                    if(perk4_plantkeuze === "radijs"){
                      setTimeout(growPerk_plant4_2, 2000);
                    }
                    if(perk4_plantkeuze === "ui"){
                      setTimeout(growPerk_plant4_3, 2000);
                    }
                    if(perk4_plantkeuze === "aardappel"){
                      setTimeout(growPerk_plant4_4, 2000);
                    }

                }
                if (water_gegeven4) {
                    console.log("Dit perkje heeft geen water meer nodig.");
                }
                if (water_geven4 == false) {
                    console.log("Je hebt een gieter nodig om water te geven.");
                }
            }
        }

    };
}());
