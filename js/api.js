var apiCall = 'https://api.openweathermap.org/data/2.5/weather?q=Leiden&appid=8f36c76b9bdb9d301336fba4300e88db';
$.getJSON(apiCall, weatherCallBack)

function weatherCallBack(weatherData) {
  var stadnaam = weatherData.name;
  var beschrijving = weatherData.weather[0].description;
  var weer = document.getElementById("weer");
  var scene = document.getElementById("js--scene");

  $('.weer2').append("het weer in " + stadnaam + " is nu " + beschrijving);

  if(beschrijving == "overcast clouds") {
    weer.setAttribute("environment", {
      skyColor: "#838a8e",
      horizonColor: "#314d5b"
    });
  }
  else if(beschrijving == "broken clouds") {
    weer.setAttribute("environment", {
      skyColor: "#a3acb0",
      horizonColor: "#a3acb0"
    });
  }
  else if(beschrijving == "few clouds") {
    weer.setAttribute("environment", {
      skyColor: "#a3acb0",
      horizonColor: "#a3acb0"
    });
  }
  else if(beschrijving == "scattered clouds") {
    weer.setAttribute("environment", {
      skyColor: "#E0E0E0",
      horizonColor: "#7297B2"
    });
  }
  else if(beschrijving == "clear Sky") {
    weer.setAttribute("environment", {
      skyColor: "#ffb90f",
      horizonColor: "#40dbdb"
    });
  }
  else if(beschrijving == "light intensity drizzle") {
    weer.setAttribute("environment", {
      skyColor: "#598194",
      horizonColor: "#598194"
    });
    scene.setAttribute("rain", "visible");
  }
  else if(beschrijving == "drizzle") {
    weer.setAttribute("environment", {
      skyColor: "#598194",
      horizonColor: "#598194"
    });
    scene.setAttribute("rain", "visible");
  }
  else if(beschrijving == "light rain") {
    weer.setAttribute("environment", {
      skyColor: "#1093af",
      horizonColor: "#598194"
    });
    scene.setAttribute("rain", "visible");
  }
  else if(beschrijving == "light intensity shower rain") {
    weer.setAttribute("environment", {
      skyColor: "#1093af",
      horizonColor: "#1093af"
    });
    scene.setAttribute("rain", "visible");
  }
  else if(beschrijving == "shower rain") {
    weer.setAttribute("environment", {
      skyColor: "#1093af",
      horizonColor: "#1093af"
    });
    scene.setAttribute("rain", "visible");
  }

}
